const soma = (...args) => {
    var aux = 0
    args.forEach(e => {
        aux = aux + e
    })

    return aux
}

module.exports = {
    soma
}